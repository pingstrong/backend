<?php
//app接口api

use Illuminate\Http\Request;

//接入层
Route::prefix("access")->group(function(){
    Route::post("login", "AccessController@login")->name("wap.access.login");
    Route::post("logout", "AccessController@logout")->name("wap.access.logout");
    Route::post("refresh_token", "AccessController@refreshToken")->name("wap.access.refresh_token");
    Route::any("logintip", "AccessController@loginTip")->name("wap.login.tip");
});
//Route::resource("access", "AccessController");

//已授权路由
Route::middleware('wap_auth')->group(function(){

        //Route::get("/aa", );
        Route::any("member", "MemberController@index");
        //Route::resource("member", "MemberController");

});