<?php
// +----------------------------------------------------------------------
// | [ Laravel快速后台开发 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2019 http://www.google.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: thinkpanax
// +----------------------------------------------------------------------

/*****************************************杂项路由***********************************************/
//安装
Route::prefix('thinkpanax/install')->group(function ($route) {
    $route->any('/', 'Install\IndexController@index')->name('thinkpanax.install');
    $route->any('/test', 'Install\IndexController@test')->name('thinkpanax.test');
});




//验证码
Route::prefix('api/')->group(function ($route) {
    $route->get('captcha/{type?}', 'Api\CaptchaController@index')->name('api.captcha');

});



/*****************************************END杂项路由***********************************************/



//未能匹配的路由

Route::fallback(function(){

    $route = Route::current();

    $name = Route::currentRouteName();

    $action = Route::currentRouteAction();

    return now() . ":" . request()->url() . "/ip:" . request()->ip();
    //return dd($route->getPrefix(), $action);
});