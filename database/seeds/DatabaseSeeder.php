<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(AdminInitTableSeeder::class);//管理员数据
         //$this->call(MemberTableSeeder::class); //

    }
}
