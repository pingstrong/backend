<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Auth\AuthenticationException;


class WapAuthenticate extends Authenticate
{

    /**
     * 授权检测
     *
     * @param [type] $request
     * @param array $guards
     * @return void
     */
    protected function authenticate($request, array $guards)
    {
        $guard = 'wap';
        if ($this->auth->guard($guard)->check()) {
            return $this->auth->shouldUse($guard);
        }

        throw new AuthenticationException(
            'Unauthenticated.', [$guard], $this->redirectTo($request)
        );
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {

        return route("wap.login.tip");
    }


}