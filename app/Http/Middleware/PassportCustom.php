<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;

class PassportCustom
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $params = $request->all();
        $provider = array_key_exists('provider', $params) ? $params['provider'] :  'wap' ;

        Config::set('auth.guards.api.provider', $provider);  // 动态配置 auth.guards.api.provider 的 model

        return $next($request);
    }
}
