<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    //
    use SoftDeletes, HasApiTokens, Notifiable;

    protected $fillable = [
        'account', 'password','remember_token'
    ];

    protected $hidden = [
        'password','remember_token'
    ];

    /**
     * passport 认证时会通过该方法查找数据，修改该方法可实现通过其它表或非 email 字段登录
     * @param $username
     * @return mixed
     */
    public function findForPassport($username)
    {
        return Member::where('account', $username)->first();
    }

    /**
    * 通过Passport的密码授权验证用户使用的密码。
    *
    * @param  string $password
    * @return bool
    */
    public function validateForPassportPasswordGrant($password)
    {
        return Hash::check($password, $this->password);
    }


}
