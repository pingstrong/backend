<?php
// +----------------------------------------------------------------------
// | Thinkpaanx [  Laravel快速后台开发 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2019 http://www.google.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: wsp <1328559667@qq.com>`
// +----------------------------------------------------------------------
namespace App\Models;

use App\TraitClass\SearchScopeTrait;


class Permission extends \Spatie\Permission\Models\Permission
{

    use SearchScopeTrait;

    protected $guarded = [];


}
