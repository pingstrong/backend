<?php
return [
    'system_name' => '管理后台',
    'system_version' => '1.0.2',
    'author' => 'wsp',
    'qq'=>'1328559667',
    'mobile'=>'Facebooke',
    'system_desc'=>'精简数据后台',
    'domain' => 'www.qq.com',
    'system_domain'=>'qq.com',
    'installs' => ['安装协议', '环境检测', '参数设置', '开始安装', '完成安装'],
    'site_name'=>'Data Admin',
    'feature'=>'快速后台开发，包含了增删改查，图片上传，数据导入/导出，系统配置，权限RBAC等，前端使用Layui，上手更容易',
    'mark_url'=>'//google.com/'
];
?>