@extends('admin.layout.base')
@section('content')
    <div class="layui-row layui-col-space15">
        <div class="layui-card">
            <div class="layui-card-header">版本信息</div>
            <div class="layui-card-body layui-text">
                <table class="layui-table">
                    <colgroup>
                        <col width="100">
                        <col>
                    </colgroup>
                    <tbody>
                    <tr>
                        <td>
                            系统名字
                        </td>
                        <td>
                            @thinkpanax('system_name') (@thinkpanax('system_desc'))
                        </td>
                    </tr>
                    <tr>
                        <td>当前版本</td>
                        <td>
                            <script type="text/html" template="">


                            </script>
                            @thinkpanax('system_version')
                        </td>
                    </tr>
                    <tr>
                        <td>基于框架</td>
                        <td>
                            PHP + Laravel {{ app()->version() }}

                        </td>
                    </tr>
                    <tr>
                        <td>主要特色</td>
                        <td>@thinkpanax('feature')</td>
                    </tr>
                    <tr>
                        <td>许可</td>
                        <td style="padding-bottom: 0;">

                            准许MIT协议，允许你重新修改和包装，但需要保留版权信息。

                        </td>
                    </tr>
                    <tr>
                        <td>作者</td>
                        <td style="padding-bottom: 0;">

                            @thinkpanax('author')

                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="layui-row layui-col-space15">
        <div class="layui-card">
            <div class="layui-card-header">插件</div>
            <div class="layui-card-body layui-text">

            </div>
        </div>
    </div>
@endsection